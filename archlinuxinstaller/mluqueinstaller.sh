#!/bin/bash

timedatectl set-ntp true
timedatectl status

# partitioning, swap and mount partitions

mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkswap /dev/sda3
swapon /dev/sda3
mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

# install
pacstrap /mnt base linux linux-firmware

# fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Execute cat /mnt/etc/fstab to see the created fstab file.
cat <<EOF > /mnt/root/part2.sh
# stuff here to do inside the chroot
# Time zone
ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
hwclock --systohc

# Localization
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
touch /etc/locale.conf
echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
touch /etc/vconsole.conf
echo 'KEYMAP=es' >> /etc/vconsole.conf

# Network configuration
touch /etc/hostname
echo 'laptopcau2013' >> /etc/hostname
touch /etc/hosts
echo '127.0.0.1	localhost' >> /etc/hosts
echo '::1		localhost' >> /etc/hosts
echo '127.0.1.1	laptopcau2013.localdomain	laptopcau2013' >> /etc/hosts

# Install basic programs
pacman -S --noconfirm networkmanager base-devel nano efibootmgr intel-ucode

# Install boot loader
bootctl --path=/boot install
cd /boot/
cd loader/
sed -i 's/#timeout 3/timeout 3/g' loader.conf
sed -i 's/default.*/default arch-*/g' loader.conf
cd entries/
touch arch.conf
echo 'title	Arch Linux' >> arch.conf
echo 'linux	/vmlinuz-linux' >> arch.conf
echo 'initrd	/intel-ucode.img' >> arch.conf
echo 'initrd	/initramfs-linux.img' >> arch.conf
echo 'options	root=/dev/sda2 rw' >> arch.conf
bootctl --path=/boot update

# Enable NetworkManager
cd
systemctl enable NetworkManager
# exit to leave the chroot
exit
EOF

# Moving to arch-chroot
chmod +x /mnt/root/part2.sh
arch-chroot /mnt ./root/part2.sh
